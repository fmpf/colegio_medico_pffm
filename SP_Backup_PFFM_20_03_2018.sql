USE master
GO
CREATE PROC BACKUP_ALL_DB_PARENTRADA
@path VARCHAR(256)
AS
DECLARE @name VARCHAR(50), -- database name
-- @path VARCHAR(256), -- path for backup files
@fileName VARCHAR(256), -- filename for backup
@fileDate VARCHAR(20), -- used for file name
@backupCount INT

CREATE TABLE [dbo].#tempBackup (intID INT IDENTITY (1, 1), name VARCHAR(200))

-- Crear la Carpeta Backup
-- SET @path = 'C:\Backup\'

-- Includes the date in the filename
SET @fileDate = CONVERT(VARCHAR(20), GETDATE(), 112)

-- Includes the date and time in the filename
--SET @fileDate = CONVERT(VARCHAR(20), GETDATE(), 112) + '_' + REPLACE(CONVERT(VARCHAR(20), GETDATE(), 108), ':', '')

INSERT INTO [dbo].#tempBackup (name)
SELECT name
FROM master.dbo.sysdatabases
WHERE name in ( 'Francisco_Pampin_20_03_2018','colegio_medico_PFFM')
-- WHERE name NOT IN ('master', 'model', 'msdb', 'tempdb')

SELECT TOP 1 @backupCount = intID 
FROM [dbo].#tempBackup 
ORDER BY intID DESC

-- Utilidad: Solo Comprobaci�n N� Backups a realizar
print @backupCount

IF ((@backupCount IS NOT NULL) AND (@backupCount > 0))
BEGIN
DECLARE @currentBackup INT

SET @currentBackup = 1
WHILE (@currentBackup <= @backupCount)
BEGIN
SELECT
		@name = name,
		@fileName = @path + name + '_' + @fileDate + '.BAK' -- Unique FileName
--@fileName = @path + @name + '.BAK' -- Non-Unique Filename
FROM [dbo].#tempBackup
WHERE intID = @currentBackup

-- Utilidad: Solo Comprobaci�n Nombre Backup
print @fileName

-- does not overwrite the existing file
BACKUP DATABASE @name TO DISK = @fileName
-- overwrites the existing file (Note: remove @fileDate from the fileName so they are no longer unique
--BACKUP DATABASE @name TO DISK = @fileName WITH INIT

SET @currentBackup = @currentBackup + 1
END
END

-- Utilidad: Solo Comprobaci�n Mirar panel de Resulatados Autonumerico y Nombre BD
select * from [dbo].#tempBackup

DROP TABLE [dbo].#tempBackup

GO


-- Ejecutar Procedimiento

EXEC BACKUP_ALL_DB_PARENTRADA 'C:\Backuppffm\'