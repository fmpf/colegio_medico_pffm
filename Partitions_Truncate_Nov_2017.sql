-- https://docs.microsoft.com/es-es/sql/t-sql/functions/partition-transact-sql

DROP DATABASE IF EXISTS powerSQLPartitionTest;
GO
use master
go
CREATE DATABASE powerSQLPartitionTest;
GO
USE powerSQLPartitionTest
GO
--The following statements create filegroups to a database powerSQLPartitionTest
---- 
ALTER DATABASE powerSQLPartitionTest ADD FILEGROUP [Filegroup_2017]  
GO  
ALTER DATABASE powerSQLPartitionTest ADD FILEGROUP [Filegroup_2018]  
GO  
ALTER DATABASE powerSQLPartitionTest ADD FILEGROUP [Filegroup_2019]  
GO  
ALTER DATABASE powerSQLPartitionTest ADD FILEGROUP [Filegroup_2020]  
GO
ALTER DATABASE powerSQLPartitionTest ADD FILEGROUP [Filegroup_2021]  
GO
-- Add one file to each filegroup so that you can store partition data in each filegroup
---- 
ALTER DATABASE powerSQLPartitionTest
  ADD FILE
  (
NAME = 'data_2017',
FILENAME = 'c:\DATA\data_2017.ndf',
SIZE = 5MB,
MAXSIZE = 100MB,
FILEGROWTH = 2MB
  )
  TO FILEGROUP [Filegroup_2017]
GO  
ALTER DATABASE powerSQLPartitionTest
  ADD FILE
  (
NAME = 'data_2018',
FILENAME = 'c:\DATA\data_2018.ndf',
SIZE = 5MB,
  	MAXSIZE = 100MB,
  	FILEGROWTH = 2MB
  )
  TO FILEGROUP [Filegroup_2018]
GO
ALTER DATABASE powerSQLPartitionTest
  ADD FILE
  (
NAME = 'data_2019',
  	FILENAME = 'c:\DATA\data_2019.ndf',
  	SIZE = 5MB,
  	MAXSIZE = 100MB,
FILEGROWTH = 2MB
  )
  TO FILEGROUP [Filegroup_2019]
GO  
ALTER DATABASE powerSQLPartitionTest
  ADD FILE
  (
NAME = 'data_2020',
  	FILENAME = 'c:\DATA\data_2020.ndf',
  	SIZE = 5MB,
  	MAXSIZE = 100MB,
  	FILEGROWTH = 2MB
  )
  TO FILEGROUP [Filegroup_2020]
GO  
ALTER DATABASE powerSQLPartitionTest
  ADD FILE
  (
NAME = 'data_2021',
  	FILENAME = 'c:\DATA\data_2021.ndf',
  	SIZE = 5MB,
  	MAXSIZE = 100MB,
  	FILEGROWTH = 2MB)
  TO FILEGROUP [Filegroup_2021]
GO  
-----
SELECT *
FROM sys.filegroups  
GO

--name		data_space_id	type	type_desc	is_default	is_system	filegroup_guid	log_filegroup_id	is_read_only	is_autogrow_all_files
--PRIMARY			1			FG		ROWS_FILEGROUP	1	0	NULL	NULL	0	0
--Filegroup_2017	2	FG	ROWS_FILEGROUP	0	0	74DF3562-0801-4210-A0E2-1E14D9D827AF	NULL	0	0
--Filegroup_2018	3	FG	ROWS_FILEGROUP	0	0	41B207A1-4F7B-4847-A1CE-690CF1F87A0D	NULL	0	0
--Filegroup_2019	4	FG	ROWS_FILEGROUP	0	0	B6C0ADAA-7DF8-4855-AB17-D0C3C218702D	NULL	0	0
--Filegroup_2020	5	FG	ROWS_FILEGROUP	0	0	C092FEFB-0777-42D9-9CB0-986E0940EB3A	NULL	0	0
--Filegroup_2021	6	FG	ROWS_FILEGROUP	0	0	0B532BE7-4CC4-4209-8CD7-26C050C94B2E	NULL	0	0


SELECT *
FROM sys.database_files --vemos los archivos f�sicos
GO

--file_id	file_guid	type	type_desc	data_space_id	name	physical_name	state	state_desc	size	max_size	growth	is_media_read_only	is_read_only	is_sparse	is_percent_growth	is_name_reserved	create_lsn	drop_lsn	read_only_lsn	read_write_lsn	differential_base_lsn	differential_base_guid	differential_base_time	redo_start_lsn	redo_start_fork_guid	redo_target_lsn	redo_target_fork_guid	backup_lsn
--1	63C95898-9AB3-4495-8DB4-95971ED4B5BB	0	ROWS	1	powerSQLPartitionTest	C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\powerSQLPartitionTest.mdf	0	ONLINE	1024	-1	8192	0	0	0	0	0	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--2	CDE9BA77-66EB-4622-B185-1A08B808D349	1	LOG	0	powerSQLPartitionTest_log	C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\powerSQLPartitionTest_log.ldf	0	ONLINE	1024	268435456	8192	0	0	0	0	0	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--3	A0F03464-3934-4902-989E-EC80108B569C	0	ROWS	2	data_2017	c:\DATA\data_2017.ndf	0	ONLINE	640	12800	256	0	0	0	0	0	34000000035300001	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--4	7424743B-D952-422F-9377-D88756E4E8AC	0	ROWS	3	data_2018	c:\DATA\data_2018.ndf	0	ONLINE	640	12800	256	0	0	0	0	0	34000000038000001	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--5	92512A49-03B5-4883-9AA1-04F078EBD954	0	ROWS	4	data_2019	c:\DATA\data_2019.ndf	0	ONLINE	640	12800	256	0	0	0	0	0	34000000040700001	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--6	DA4F3B44-899C-4BA4-935E-F1290A536C9D	0	ROWS	5	data_2020	c:\DATA\data_2020.ndf	0	ONLINE	640	12800	256	0	0	0	0	0	34000000043400001	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--7	B2680180-EBDA-4840-A704-2E0554552E6F	0	ROWS	6	data_2021	c:\DATA\data_2021.ndf	0	ONLINE	640	12800	256	0	0	0	0	0	34000000046100001	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL


---Create Partition Range Function as follows
 
CREATE PARTITION FUNCTION powerSQLPartitionTest_PartitionRange (INT)
	AS RANGE RIGHT FOR VALUES (10,20,30,40,50);
GO

-- Todos los datos hasta 10 10 incluido
-- 	-10 10-20 20-30 30-40 40-50 50-
----Mapping partition scheme filegroups to the partition range function 
 
CREATE PARTITION SCHEME powerSQLPartitionTest_PartitionScheme
AS PARTITION powerSQLPartitionTest_PartitionRange TO ([PRIMARY], [Filegroup_2017],Filegroup_2018,Filegroup_2019,Filegroup_2020,Filegroup_2021);
GO


----------
--NOTE: Generally, I recommend RIGHT-based partition function so that you don't have to deal with datetime timetick issues at all. However, this post can really help you if you still want to use LEFT-based partition functions. Enjoy!!! 

--When creating partitioned tables in SQL Server 2005, a partition function requires a LEFT or RIGHT designation. In general, I recommend that you choose to create a LEFT-based partition. However, choosing LEFT always creates some confusion because a left-based partition function requires upper boundaries. This results in a more complicated partition function definition (than a RIGHT partition function).

--RANGE RIGHT FOR VALUES (
--   '20040701', � Jul 2004
--   '20040801', � Aug 2004
--   '20040901' � Sep 2004
--)

--This will create 4 partitions where the partitions will be:
-- Partition 1: all data less than 20040701
-- Partition 2: all data greater then/equal to 20040701 and less than 20040801
-- Partition 3: all data greater then/equal to 20040801 and less than 20040901
-- Partition 4: all data greater then/equal to 20040901

-- -----------------------------
-- Map
--	10		20		30		40	50
-- [PRIMARY], [Filegroup_2017],Filegroup_2018,Filegroup_2019,Filegroup_2020,Filegroup_2021


--Now that there is a partition function and scheme, you can create a partitioned table.
-- The syntax is very similar to any other CREATE TABLE statement except it references 
--the partition scheme instead of a referencing filegroup
 
CREATE TABLE powerSQLPartitionTestTable
(ID INT NOT NULL,
Date DATE default getdate())
ON powerSQLPartitionTest_PartitionScheme (ID);
GO
 
--Now that the table has been created on a partition scheme powerSQLPartitionTestTable ,
-- populate table using sample data
 
Insert into powerSQLPartitionTestTable (ID)
	SELECT r_Number
	FROM (
	SELECT ABS(CAST(NEWID() AS binary(6)) %1000) + 1 r_Number
	FROM master..spt_values) sample
	GROUP BY r_Number
	ORDER BY r_Number
GO

-- (932 rows affected)


--- select Data from powerSQLPartitionTestTable
 
SELECT *
FROM powerSQLPartitionTestTable;
GO


SELECT count(*)
FROM powerSQLPartitionTestTable
WHERE id<10
GO
-- 9


SELECT count(*)
FROM powerSQLPartitionTestTable
WHERE id>=50
GO

